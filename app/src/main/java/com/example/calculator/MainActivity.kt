package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    private lateinit var expression: TextView
    private lateinit var expressionStr: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        expression = findViewById(R.id.expression)
    }



    fun writeExpression(clickedView: View) {
        if (clickedView is TextView) {
            expressionStr = expression.text.toString()
            val num: String = clickedView.text.toString()

            if(expressionStr == "0" && clickedView.text.toString() != ".") {
                expressionStr = ""
            }

            expression.text = expressionStr + num
        }
    }


    fun operateNum(clickedView: View) {
        expressionStr = expression.text.toString()
        if (clickedView is TextView && expressionStr != "") {

            val lastChar: String = expression.text[expression.text.length - 1].toString()

            if(lastChar != "." && lastChar != "+" && lastChar != "-" && lastChar != "*" && lastChar != "/") {
                expression.text = expressionStr + clickedView.text.toString()
            } else {
                expression.text = expressionStr.dropLast(1) + clickedView.text.toString()
            }

        }
    }


    fun calculate(clickedView: View) {
        if (clickedView is TextView) {
            expressionStr = expression.text.toString()

            try {
                val expressionBuilder = ExpressionBuilder(expressionStr).build()
                var answer = expressionBuilder.evaluate().toString()
                if(answer[answer.length - 1].toString() == "0" && answer[answer.length - 2].toString() == ".") {
                    answer = answer.dropLast(2)
                }
                expression.text = answer.toString()
            } catch (e:Exception) {
                Toast.makeText(applicationContext, "Invalid input", LENGTH_SHORT).show()
            }

        }
    }


    fun delete(clickedView: View) {
        if (clickedView is TextView) {
            expressionStr = expression.text.toString()
            expression.text = expressionStr.dropLast(1)
        }
    }


    fun clear(clickedView: View) {
        if (clickedView is TextView) {
            expression.text = ""
        }
    }
}